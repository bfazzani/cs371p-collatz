var indexSectionsWithContent =
{
  0: "abcgmrt",
  1: "g",
  2: "cgrt",
  3: "cgmt",
  4: "abc",
  5: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "namespaces",
  2: "files",
  3: "functions",
  4: "variables",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Namespaces",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Pages"
};

