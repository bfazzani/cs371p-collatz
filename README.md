# CS371p: Object-Oriented Programming Collatz Repo

* Name: Bruno Fazzani

* EID: bf8526

* GitLab ID: bfazzani

* HackerRank ID: bfazzani

* Git SHA: 0b18d41953d936a8a9b5f606a0797897711d26ca

* GitLab Pipelines: https://gitlab.com/bfazzani/cs371p-collatz/-/pipelines

* Estimated completion time: 10 hrs

* Actual completion time: 4 hrs

* Comments:
Project took much less time than I thought it would. I thought I would get
bogged down by all the new tools I was supposed to use, but the given makefile
meant that everything was very easy and straightforward.