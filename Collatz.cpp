// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

// used an array for the cache because it is very easy to index with a number
// and is very lightweight/fast
int cache[1000000];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;

    assert (i > 0 && j > 0 && i < 1000000 && j < 1000000);

    cache[1] = 1;
    int max_cycle_length = -1;

    int low = min(i, j);
    int high = max(i, j);
    int mid = (high / 2) + 1;
    if (low < mid) {
        low = mid;
    }

    assert (low <= high);
    for (int start=low; start <= high; start++) {
        int cycle_length = get_cycle_length(start);
        max_cycle_length = max(max_cycle_length, cycle_length);
    }

    return make_tuple(i, j, max_cycle_length);
}

long get_cycle_length(long n) {
    // need to use long for n because intermediate values sometimes overflow.
    // the cache can still be int though because the actual cycle lengths never
    // go more than 10^3
    assert (n > 0);
    if (n >= 1000000) {
        if (n % 2 == 0) {
            return get_cycle_length(n / 2) + 1;
        } else {
            // not 3*n + 1 because using the even/odd optimization
            return get_cycle_length (n + (n >> 1) + 1) + 2;
        }
    }

    assert (n < 1000000);
    if (cache[n] == 0) {
        if (n % 2 == 0) {
            cache[n] = get_cycle_length(n / 2) + 1;
        } else {
            // not 3*n + 1 because using the even/odd optimization
            cache[n] = get_cycle_length(n + (n >> 1) + 1) + 2;
        }
    }
    return cache[n];
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
